import random as random
from collections.abc import Sequence
from itertools import chain, count
HitOnSoft = 1
H17 = 1
Pen = 52
Unit = 25
NumOfDecks = 6
onedeck = [2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,11,11]
People = 2
RunningCount = 0
HardTotals = [ [0,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,1,1,1,1],
               [0,0,0,0,0,1,1,1,1,1],
               [0,0,0,0,0,1,1,1,1,1],
               [0,0,0,0,0,1,1,1,1,1],
               [1,1,0,0,0,1,1,1,1,1],
               [2,2,2,2,2,2,2,2,2,2],
               [2,2,2,2,2,2,2,2,1,1],
               [1,2,2,2,2,1,1,1,1,1],
               [1,1,1,1,1,1,1,1,1,1]]
Splitting =   [[1,1,1,1,1,1,1,1,1,1],
               [0,0,0,0,0,0,0,0,0,0],
               [1,1,1,1,1,0,1,1,0,0],
               [1,1,1,1,1,1,1,1,1,1],
               [1,1,1,1,1,1,0,0,0,0],
               [1,1,1,1,1,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,0],
               [0,0,0,1,1,0,0,0,0,0],
               [1,1,1,1,1,1,0,0,0,0],
               [1,1,1,1,1,1,0,0,0,0]]
SoftTotal =   [[0,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,3,0,0,0,0,0],
               [3,3,3,3,3,0,0,1,1,1],
               [1,2,2,2,2,1,1,1,1,1],
               [1,1,2,2,2,1,1,1,1,1],
               [1,1,2,2,2,1,1,1,1,1],
               [1,1,1,2,2,1,1,1,1,1],
               [1,1,1,2,2,1,1,1,1,1]]

def main():
    cash = []
    win = []
    sidebet = []
    for x in range(100000):
        cashing, winning, side = multiround(100)
        cash.append(cashing)
        win.append(winning)
        sidebet.append(side)
        # print("Win rate: ", (sum(win) / len(win)))
        # print(sum(sidebet) / len(sidebet))
        # print("Number of Decks: ",NumOfDecks)
        # print("Penetration: ", Pen)
        #
        print("Total Cash: ",sum(cash))
        # print("Cash per hour: ",(sum(cash)/len(cash)))


def multiround(Times):
    global RunningCount
    Deck = shuffle()
    Players = players(People)
    AllWin = 0
    AllLoss=0
    AllTie=0
    AllAdd=0
    AllCash=0
    Win = 0
    Lose = 0
    TrueWin = []
    sidebet = 0
    total = 0
    for x in range(Times):
        WinRate, LossRate, TieRate, AdditionalHands, Cash, win, lose, sidebet = round(Deck, Players)
        AllWin += WinRate
        AllLoss += LossRate
        AllTie += TieRate
        AllAdd += AdditionalHands
        AllCash += Cash
        Players = players(People)
        Win += win
        Lose += lose
        total += sidebet

        if len(Deck) < Pen:
            RunningCount = 0
            Deck = shuffle()
        # print(len(Deck))
        # print(Cash)
        # print(AllCash)
        # print()
        # if AllCash < -1000:
        #     print(AllCash)
    Hours = Times/100
    # print((AllWin/(Times+AllAdd)), (AllLoss/(Times+AllAdd)), AllCash)
    # print(AllCash/Hours)
    # print()

    return AllCash, WinRate/(Times+AllAdd-TieRate), total

def bet(Deck):
    TrueCount = RunningCount//(len(Deck)//52) -1
    if TrueCount < 1:
        Bet = 10

    else:
        Bet = TrueCount * Unit + Unit
    # print(TrueCount)
    if Bet > (4*Unit):
        Bet = 4*Unit
    return Bet, TrueCount
def round(Deck, Players):
    lose = 0
    win = 0
    sidebet = 0
    Bet, CurrentTrueCount= bet(Deck)

    Hands, Deck = deal(Players, Deck)

    # print(Bet)
    # print(Hands)
    Hands, Deck = gamelogic(Hands, Deck)
    # print(Hands)
    Hand = Hands.pop(0)
    Dealer = Hands.pop(-1)
    if CurrentTrueCount > 2 and Dealer[1] == 11:
        if sum(Dealer[:2]) == 21:
            sidebet = Bet
        else:
            sidebet = -Bet
    WinRate, LossRate, TieRate, AdditionalHands, Cash = results(Hand, Dealer, Bet)
    if CurrentTrueCount > 1:
        if WinRate > 0:
            win = 1
        if LossRate > 0:
            lose = 1



    #
    # print(WinRate, LossRate, Bet, Cash)
    # print(RunningCount)
    # print()
    Cash += sidebet

    return WinRate, LossRate, TieRate, AdditionalHands, Cash, win, lose, sidebet
def results(Hands, Dealer, Bet):
    WinRate = 0
    TieRate = 0
    LossRate = 0
    AdditionalHands = 0
    Cash = 0
    Rate = 1
    if Hands[1] == 0:
        Rate = 2

    if depth(Hands) != 1:
        AdditionalHands += -1
        for Hand in Hands:
            if sum(Hand) == 21 and len(Hand) < 3 and Dealer != 21:
                WinRate += 1.5
                Cash += Bet*1.5

            elif sum(Hand) > 21:
                LossRate += 1
                Cash -= Bet
            elif sum(Dealer) > 21:
                WinRate += 1
                Cash += Bet
            elif sum(Hand) > sum(Dealer):
                WinRate += 1
                Cash += Bet
            elif sum(Hand) == sum(Dealer):
                TieRate += 1
            else:
                LossRate += 1
                Cash -= Bet
    else:
        Hand = Hands
        if sum(Hand) == 21 and len(Hand) < 3 and sum(Dealer[:2]) != 21:
            WinRate += 1.5
            Cash += Bet * 1.5
        elif sum(Hand) > 21:
            LossRate += 1
            Cash -= Bet
        elif sum(Dealer) > 21:
            WinRate += 1
            Cash += Bet
        elif sum(Hand) > sum(Dealer):
            WinRate += 1
            Cash += Bet
        elif sum(Hand) == sum(Dealer):
            TieRate += 1
        else:
            LossRate += 1
            Cash -= Bet
    return (WinRate*Rate), (LossRate*Rate), (TieRate*Rate), (AdditionalHands*Rate), (Cash*Rate)



def depth(seq):
    seq = iter(seq)
    try:
        for level in count():
            seq = chain([next(seq)], seq)
            seq = chain.from_iterable(s for s in seq if isinstance(s, Sequence))
    except StopIteration:
        return level

def gamelogic(Hands, Deck):
    all = []
    DealersHand = Hands.pop(-1)
    Dealer = DealersHand[1]


    DealerCase = 0
    for x in range(len(Hands)):
        Carry = [0]
        hold = Hands.pop(0)
        if sum(DealersHand) != 21:
            hold, Deck, Dealer, Carry = call(hold, Deck, Dealer, Carry)
        Carry.pop(0)
        if len(Carry) != 0:
            hold = Carry
        all.append(hold)
        if depth(hold) > 1:
            for x in hold:
                try:
                    if sum(x) < 22:
                        DealerCase += 1
                except:
                    print(1)
        else:
            if sum(hold) < 22:
                DealerCase += 1
    if DealerCase != 0:

        if sum(DealersHand) == 22:
            hold = aceto1(DealersHand)
        if HitOnSoft == 1:
            if (sum(DealersHand) == 17) and (11 in DealersHand): DealersHand = aceto1(DealersHand)
        while sum(DealersHand) < 17:
            DealersHand, Deck = hit(DealersHand, Deck)
            if sum(DealersHand) > 21:
                if 11 in DealersHand:
                    # print(hold)
                    DealersHand = aceto1(DealersHand)
            if HitOnSoft == 1:
                if sum(DealersHand) == 17and (11 in DealersHand): DealersHand = aceto1(DealersHand)
    all.append(DealersHand)

    # print(all)
    return all, Deck

def splithandler(Hands, Deck, Dealer, Carry): # Max 4 splits, Aces split 1
    if Hands[0] == 11 and Hands[1] == 11 and Carry[0]<4:
        Carry[0] =  4
        Hands, Deck, Dealer, Carry = split(Hands, Deck, Dealer, Carry)
        return Hands, Deck, Dealer, Carry
    elif Carry[0]<4:
        Carry[0] = Carry[0] + 1
        Hands, Deck, Dealer, Carry = split(Hands, Deck, Dealer, Carry )
        return Hands, Deck, Dealer, Carry
    else:
        return Hands, Deck, Dealer, Carry

def split(Hands, Deck, Dealer, Carry ):
    hand1 = []
    hand2 = []
    hand1.append(Hands[0])
    hand2.append(Hands[1])
    hand1, Deck = hit(hand1,Deck)
    hand2, Deck = hit(hand2,Deck)
    Hands, Deck, Dealer, Carry = call(hand1,Deck,Dealer,Carry)
    Hands, Deck, Dealer, Carry = call(hand2,Deck,Dealer,Carry)

    if len(hand1) != 0: Carry.append(hand1)
    if len(hand2) != 0 :Carry.append(hand2)
    return Hands,Deck, Dealer, Carry
def double(Hand):
    Hand[0] = Hand[0] + Hand[1]
    Hand[1] = 0
    if sum(Hand) > 21 and 11 in Hand[1:]:
        Hand = aceto1(Hand)
    return Hand
def call(Hands,Deck, Dealer, Carry):
    if sum(Hands) == 21:
        return Hands, Deck, Dealer, Carry
    else:
        if Hands[0] == Hands[1]:
            Case = Splitting[11-Hands[0]][Dealer - 2]
            if Case == 1:
                Hands,Deck, Dealer, Carry = splithandler(Hands,Deck, Dealer, Carry)
                return  Hands, Deck, Dealer, Carry

        if 11 in Hands:
            Case = SoftTotal[9-(sum(Hands)-11)][Dealer - 2]
            if Case == 0:
                return Hands, Deck, Dealer, Carry
            if Case == 2 or Case == 3:
                Hands, Deck = hit(Hands, Deck)
                if sum(Hands) > 21 :
                    Hands = aceto1(Hands)
                Hands = double(Hands)
                return Hands, Deck, Dealer, Carry
        while 1==1:

            if sum(Hands) < 8:
                Hands, Deck = hit(Hands,Deck)
            elif sum(Hands)>21:
                if 11 in Hands:
                    Hands = aceto1(Hands)
                else:
                    return Hands, Deck, Dealer, Carry
            elif 11 in Hands:
                Case = SoftTotal[9-(sum(Hands)-11)][Dealer - 2]
                if Case == 0 or Case == 3:
                    return  Hands, Deck, Dealer, Carry
                else:
                    Hands, Deck = hit(Hands,Deck)
            elif sum(Hands)<18:
                Case = HardTotals[17-sum(Hands)][Dealer - 2]
                if Case == 4 and RunningCount > 0:
                    return Hands, Deck, Dealer, Carry
                elif Case == 4:
                    Case = 1
                if Case == 2:
                    if len(Hands) >2:
                        Case = 1
                if Case == 0:
                    return Hands, Deck, Dealer, Carry
                elif Case == 1: Hands, Deck = hit(Hands,Deck)
                elif Case == 2:
                    Hands, Deck = hit(Hands,Deck)
                    Hands = double(Hands)
                    return Hands, Deck, Dealer, Carry
            else:
                return Hands, Deck, Dealer, Carry

def deal(hands,deck):
    # print(deck)
    global RunningCount
    for y in range(2):
        for x in range(len(hands)):
            card = deck.pop(0)
            RunningCount += cardvalue(card)
            # print(running_count)
            hands[x].append(card)
    # print(hands)
    # print(len(cards))
    return hands, deck

def cardvalue(card):
    if card == 1 or card == 2 or card == 3 or card == 4 or card == 5 or card == 6:
        return 1
    if card == 10 or card == 11:
        return -1
    else:
        return 0

def players(people):
    seats = []
    dealer = []
    seats.append(dealer)
    for n in range(people):
        n = []
        seats.append(n)
    return seats

def shuffle():
    playing = []
    for n in range(NumOfDecks):
        playing = playing + onedeck
    random.shuffle(playing)
    random.shuffle(playing)
    # print(time.time()-tim)
    return playing

def hit(hand, deck):
    global RunningCount
    card = deck.pop(0)
    RunningCount += cardvalue(card)
    # print(running_count)
    hand.append(card)
    return (hand, deck)

def aceto1(hand):
    x = hand.index(11)
    hand[x] = 1
    return hand
if __name__ == '__main__':
    main()
    k = input("press close to exit")


import random as random
import time
from collections.abc import Sequence
from itertools import chain, count


deck = []
onedeck = [2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,11,11]
deckcount = 6
truecount = 0
pen = 1
box = 52
handsplayed = 100
sets = 10000
peopleattable = 2
running_count = 0
HardTotals = [ [0,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,1,1,1,1],
               [0,0,0,0,0,1,1,1,1,1],
               [0,0,0,0,0,1,1,1,1,1],
               [0,0,0,0,0,1,1,1,1,1],
               [1,1,0,0,0,1,1,1,1,1],
               [2,2,2,2,2,2,2,2,2,2],
               [2,2,2,2,2,2,2,2,1,1],
               [1,2,2,2,2,1,1,1,1,1],
               [1,1,1,1,1,1,1,1,1,1]]
Splitting =   [[1,1,1,1,1,1,1,1,1,1],
               [0,0,0,0,0,0,0,0,0,0],
               [1,1,1,1,1,0,1,1,0,0],
               [1,1,1,1,1,1,1,1,1,1],
               [1,1,1,1,1,1,0,0,0,0],
               [1,1,1,1,1,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,0],
               [0,0,0,1,1,0,0,0,0,0],
               [1,1,1,1,1,1,0,0,0,0],
               [1,1,1,1,1,1,0,0,0,0]]
SoftTotal =   [[0,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,3,0,0,0,0,0],
               [3,3,3,3,3,0,0,1,1,1],
               [1,2,2,2,2,1,1,1,1,1],
               [1,1,2,2,2,1,1,1,1,1],
               [1,1,2,2,2,1,1,1,1,1],
               [1,1,1,2,2,1,1,1,1,1],
               [1,1,1,2,2,1,1,1,1,1]]





def main():
    global truecount
    winrate = []

    tick = time.time()
    StartingHands = players(peopleattable)
    little = 0
    result = 0
    adding = 0
    updown = 0
    deck = builddeck(deckcount)
    for y in range(sets):
        for x in range(handsplayed):
            myhand = []

            tim = time.time()
            truecount = running_count//((len(deck)//26)*.5)

            hands, deck = deal(StartingHands, deck)
            StartingHands = players(peopleattable)
            # print(hands)
            hands, deck = gamelogic(hands,deck)
            # print(hands)
            myhand.append(hands[0])
            myhand.append(hands[-1])
            # print(myhand)
            holder, holding, holdering  = results(myhand, truecount)

            # print(holder)
            # print()
            updown += holdering
            if updown < little:
                little = updown
            adding += holding
            result += holder
            if len(deck) < (box*pen):
                deck = builddeck(deckcount)
        winrate.append(updown)
        updown = 0


    print(time.time()-tick)

    print((result/(handsplayed*sets+adding)))
    print(winrate)
    print(min(winrate))
    print(little)
    print(max(winrate))
    print((sum(winrate)))
    print((sum(winrate) /sets))





def results(hands, truecount):
    tim = time.time()
    dealer = hands.pop(-1)
    result = 0
    adding = 0
    updown = 0
    n = 0
    if truecount < 1:
        truecount = 10
    elif truecount > 3:
        truecount = 4 * 25
    else:
        truecount = truecount * 25
    for x in hands:
        if depth(x) != 1:
            adding -= 1
            for y in x:
                adding += 1

                try:
                    if sum(y) > 21:

                        updown -= 1 * truecount
                    else:
                        if sum(y) == 21 and sum(dealer) != 21 and len(y)<3:
                            result += 1
                            updown +=  1.5*truecount
                        elif sum(dealer) > 21:
                            result +=  1
                            updown += 1 * truecount
                        elif (sum(y) > sum(dealer)) :
                            result +=  1
                            updown +=  1 * truecount
                        elif (sum(y) == sum(dealer)):
                            result +=  1

                        else:
                            updown -= 1 * truecount

                except:
                    print(1)
        else:

            if sum(x)>21:
                updown -= 1 * truecount
            else:
                if sum(x) == 21 and sum(dealer) != 21 and len(x)<3:
                    result = 1
                    updown = 1.5 * truecount
                elif sum(dealer)>21:
                    updown = 1 * truecount
                    result = 1
                elif (sum(x)> sum(dealer)):
                    updown = 1 * truecount
                    result = 1

                elif (sum(x) == sum(dealer)):
                    result = 1

                else:
                    updown = -1 * truecount
        n += 1
    # print("re",(time.time()-tim))

    return result, adding, updown

def splithandler(dealerscard, hand, deck, carry): # Max 4 splits, Aces split 1
    if hand[0] == 11 and hand[1] == 11 and carry[0]<4:
        carry[0] =  4
        dealerscard, hand, deck, carry = split(dealerscard,hand,deck,carry)
        return dealerscard, hand, deck, carry
    elif carry[0]<4:
        carry[0] = carry[0] + 1
        dealerscard, hand, deck, carry = split(dealerscard, hand,deck,carry)
        return dealerscard, hand, deck, carry
    else:
        return dealerscard, hand, deck, carry



def handcase(dealerscard, hand, deck,carry,):
    splits =  { 0 : adjust, 1 : splithandler}
    softs = { 0 : returner, 1 : hit, 2 : hit}
    hards = {0 : returner, 1 : hit, 2 : hit}
    if sum(hand) == 21:
        return dealerscard,hand,deck,carry
    if sum(hand) > 21 and (11 in hand) and (len(hand)>2):
        aceto1(hand)
    if hand[0] == hand[1]:
        func = splits.get(Splitting[11-hand[0]][dealerscard-2])
        dealerscard, hand, deck, carry = func(dealerscard,hand,deck,carry)
        return  dealerscard, hand, deck, carry
    if 11 in hand:
        case = SoftTotal[20-sum(hand)][dealerscard-2]
        func = softs.get(SoftTotal[20-sum(hand)][dealerscard-2])
        hand, deck = func(hand, deck)
        if sum(hand) > 21 and (11 in hand) and (len(hand) > 2):
            aceto1(hand)
        if (case == 0) or ((case == 2) and (len(hand) == 2) ):
            return dealerscard, hand, deck, carry

        dealerscard, hand, deck, carry = handcase(dealerscard, hand, deck,carry)
        return  dealerscard, hand, deck, carry
    if sum(hand) < 8:
        hand, deck = hit(hand,deck)
        dealerscard, hand, deck, carry = handcase(dealerscard,hand,deck,carry)
        return dealerscard, hand, deck, carry
    if sum(hand) < 17:
        count = HardTotals[17-sum(hand)][dealerscard-2]
        func = hards.get(HardTotals[17-sum(hand)][dealerscard-2])
        hand, deck = func(hand, deck)

        if (count == 0) or ((count == 2) and (len(hand) == 3)):
            if (sum(hand)>21) and (11 in hand):
                hand = aceto1(hand)
            return dealerscard, hand, deck, carry
        dealerscard, hand, deck, carry = handcase(dealerscard,hand,deck,carry)
        return dealerscard, hand, deck, carry
    return dealerscard, hand, deck, carry

def returner(hand, deck):
    return hand, deck
def adjust(dealerscard, hand, deck,carry):
    hand[0] = sum(hand)
    hand[1] = 0
    dealerscard, hand, deck, carry= handcase(dealerscard,hand,deck, carry)
    return  dealerscard, hand, deck, carry

def depth(seq):
    seq = iter(seq)
    try:
        for level in count():
            seq = chain([next(seq)], seq)
            seq = chain.from_iterable(s for s in seq if isinstance(s, Sequence))
    except StopIteration:
        return level

def split(dealerscard, hand,deck,carry):
    hand1 = []
    hand2 = []
    hand1.append(hand[0])
    hand2.append(hand[1])
    hand1, deck = hit(hand1,deck)
    hand2, deck = hit(hand2,deck)
    dealerscard, hand1, deck, carry= handcase(dealerscard,hand1,deck,carry)
    dealerscard, hand2, deck, carry = handcase(dealerscard,hand2,deck,carry)

    if len(hand1) != 0: carry.append(hand1)
    if len(hand2) != 0 :carry.append(hand2)
    return dealerscard, hand, deck, carry


def gamelogic(hands, deck):
    all = []
    dealer = hands.pop(-1)
    dealerscard = dealer[1]
    dealornodeal = 0

    for x in range(len(hands)):
        carry = [0]
        hold = hands.pop(0)
        dealerscard, hold, deck, carry = handcase(dealerscard, hold, deck, carry)
        carry.pop(0)
        if len(carry) != 0:
            hold = carry
        all.append(hold)
        if depth(hold)>1:
            for x in hold:
                try:
                    if sum(x) <22:
                        dealornodeal += 1
                except:
                    print(1)
        else:
            if sum(hold)<22:
                dealornodeal += 1
    if dealornodeal != 0:

        if sum(dealer) == 22:

            hold = aceto1(dealer)
        while sum(dealer) < 17:
            dealer, deck = hit(dealer, deck)
            if sum(dealer) > 21:
                if 11 in dealer:
                    # print(hold)
                    dealer = aceto1(dealer)
            if (dealer == 17) and (11 in dealer): dealer = aceto1(dealer)
    all.append(dealer)

    # print(all)
    return all, deck

# def gamelogic(hands, deck):
#     all = []
#     dealer = hands.pop(-1)
#     dealornodeal = 0
#     for x in range(len(hands)):
#         hold = hands.pop(0)
#         print(hold)
#         if sum(hold) == 22:
#             # print(hold)
#             hold = aceto1(hold)
#         while sum(hold)<17:
#             hold, deck = hit(hold, deck)
#             if sum(hold) > 21:
#                 if 11 in hold:
#                     # print(hold)
#                     hold = aceto1(hold)
#
#         all.append(hold)
#         print(hold)
#         if sum(hold) < 22:
#             dealornodeal =+ 1
#         # print(hold)
#     if dealornodeal>0:
#         if sum(dealer) == 22:
#
#             hold = aceto1(dealer)
#         while sum(dealer) < 17:
#             dealer, deck = hit(dealer, deck)
#             if sum(dealer) > 21:
#                 if 11 in dealer:
#                     # print(hold)
#                     dealer = aceto1(dealer)
#     all.append(dealer)
#     print(dealer)
#
#     # print(all)
#     return all, deck




def builddeck(NumOfDecks): #creates a shuffled piled of cards based off of how many decks desired
    playing = []
    tim =time.time()
    for n in range(NumOfDecks):
        playing = playing + onedeck
    random.shuffle(playing)
    # print(time.time()-tim)
    return playing

def players(people):
    seats = []
    dealer = []
    seats.append(dealer)
    for n in range(people):
        n = []
        seats.append(n)
    return seats




def hit(hand, deck):
    global running_count
    card = deck.pop(-1)
    running_count = cardvalue(card)
    # print(running_count)
    hand.append(card)
    return (hand, deck)

def aceto1(hand):
    x = hand.index(11)
    hand[x] = 1
    return hand

def cardvalue(card):
    if card == 1 or card == 2 or card == 3 or card == 4 or card == 5 or card == 6:
        return 1
    if card == 10 or card == 11:
        return -1
    else:
        return 0
def deal(hands,deck):
    # print(deck)
    global running_count
    for y in range(2):
        for x in range(len(hands)):
            card = deck.pop(-1)
            running_count += cardvalue(card)
            # print(running_count)
            hands[x].append(card)
    # print(hands)
    # print(len(cards))
    return hands, deck

def depth(seq):
    seq = iter(seq)
    try:
        for level in count():
            seq = chain([next(seq)], seq)
            seq = chain.from_iterable(s for s in seq if isinstance(s, Sequence))
    except StopIteration:
        return level

if __name__ == '__main__':
    main()





